import { calcZeroLevel, calcStage } from './combinationBuilder'

export default class Combination {
  constructor(teams, bets) {
    this.teams = teams
    this.bets = bets
    this.users = []
    this.points = []
    this.preCalc()
  }

  preCalc() {
    if(this.stages) {
      return
    }

    const stages = []
    const zero = calcZeroLevel(this.teams.length, this.bets)
    stages.push(zero)

    let stage = zero
    let betIndex = 0
    while(stage.length > 1) {
      betIndex += stage.length
      stage = calcStage(stage, this.bets, betIndex)
      stages.push(stage)
    }

    this.points = new Array(betIndex + 1).fill(0)
    this.stages = stages
  }

  getTeams(stageNo) {
    const stage = this.stages[stageNo]
    if(!stage) {
      return null
    }
    return stage.map(ind => this.teams[ind])
  }

  calcPoints(userBet) {
    let points = 0
    let pointSize = 1
    let i = 0
    const { userName, bets } = userBet
    for(let stage of this.stages) {
      for(let index of stage) {
        if(index === bets[i]) {
          points += pointSize
          this.points[i]++
        }
        i++
      }
      pointSize *= 2
    }

    this.users.push({ userName, points })
  }

  print() {
    console.log(this.stages)

    for(let stage of this.stages) {
      for(let index of stage) {
        console.log(this.teams[index])
      }
    }
  }
}