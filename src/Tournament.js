import Combination from './Combination'
import { buildStages, mixBets } from './tournamentBuilder'

export default class Tournament {
  constructor(teams) {
    this.teams = teams
    this.stages = buildStages(teams)
  }

  getStage(stageNo) {
    return this.stages[stageNo || 0]
  }

  addWinner(winner, stageNo) {
    const stage = this.getStage(stageNo)
    const game = stage.filter(x => x.p1 === winner || x.p2 === winner)[0]
    game.winner = winner
    this.postWinnerUpdate(winner, stageNo)
  }

  postWinnerUpdate(winner, stageNo) {
    stageNo = stageNo || 0
    const stage = this.getStage(stageNo)
    const gameIndex = stage.findIndex(x => x.winner === winner)
    const nextGameIndex = Math.floor(gameIndex / 2)
    const side = gameIndex % 2

    const nextStage = this.getStage(stageNo + 1)    
    const game = nextStage[nextGameIndex]
    if(side) {
      game.p2 = winner
    } else {
      game.p1 = winner
    }
  }

  getGamesResults() {
    const res = []
    for(let stage of this.stages) {
      for(let game of stage) {
        if(game.winner) {
          if(game.p1 === game.winner) {
            res.push(1)
          } else {
            res.push(2)
          }
        } else {
          res.push(0)
        }
      }
    }
    return res
  }

  getAllPossibleOutcomes(users) {
    const userBets = users.map(u => {return { bets: u.getBets(), userName: u.userName }})
    const userStats = {
      user1: 0,
      user2: 0,
      draw: 0
    }

    const gameResults = this.getGamesResults()
    const notFinishedCount = gameResults.reduce((sum, r) => sum + (r ? 0 : 1), 0)
    const bets = new Array(notFinishedCount).fill(1)

    const all = []

    while(true) {
      const mixedBets = mixBets(gameResults, bets)
      const comb = new Combination(this.teams, mixedBets)
      
      for(let user of userBets) {
        comb.calcPoints(user)
      }

      const u1 = comb.users[0].points
      const u2 = comb.users[1].points
      if(true) {
        all.push(comb)
      }

      if(u1 > u2) {
        userStats.user1++
      } else if (u1 < u2) {
        userStats.user2++
      } else {
        userStats.draw++
      }
      
      if(!this.next(bets)) {
        break
      }
    }

    userStats.total = userStats.user1 + userStats.user2 + userStats.draw
    return { combinations: all, userStats }
  }

  next(indexes) {
    let i = indexes.length - 1
    indexes[i]++    
    while(indexes[i] === 3) {
      indexes[i] = 1
      i--
      if (i < 0) {
        return false
      }
      indexes[i]++
    }
    return true
  }
}