export const calcZeroLevel = (teamsCount, bets) => {
  const res = []
  const count = teamsCount / 2
  for(let i=0;i<count;i++) {
    res.push(i*2 + bets[i] - 1)
  }
  return res
}

export const calcStage = (prevStage, bets, betIndex) => {
  const res = []
  const count = prevStage.length / 2
  for(let i=0;i<count;i++) {
    const index = i*2 + bets[betIndex + i] - 1
    res.push(prevStage[index])
  }
  return res
}