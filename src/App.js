import React, { Component } from 'react';
import wc20181_8 from './data/wc2018_1_8'
import wc20181_4 from './data/wc2018_1_4'
import ResultSnap from './ResultSnap'

const CUP_1_8 = 'WC2018 1/8'
const CUP_1_4 = 'WC2018 1/4'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {cup: CUP_1_4}
  }

  getCombinations(cup) {
    if(cup === CUP_1_8) {
      return wc20181_8.fact.getAllPossibleOutcomes(wc20181_8.users)
    }
    return wc20181_4.fact.getAllPossibleOutcomes(wc20181_4.users)
  }

  render() {
    const { cup } = this.state
    const { combinations, userStats } = this.getCombinations(cup)

    const blocksContainerStyle = {
      display: 'flex',
      justifyContent: 'space-evenly',
      flexWrap: 'wrap',
      fontSize: 10,
      margin: 10
    }

    const getLinkStyle = (linkCup) => {
      return {
        fontSize: 18,
        padding: 20,
        color: linkCup === cup ? 'red' : 'black',
        cursor: linkCup === cup ? 'auto' : 'pointer'
      }
    }

    const changeCup = (cup) => {
      this.setState({ cup: cup })
    }

    const summuryStyle = {
      width: 250,
      display: 'flex',
      justifyContent: 'space-evenly',
    }

    return (
      <div>
        <div>
          <a style={getLinkStyle(CUP_1_8)} onClick={() => changeCup(CUP_1_8)}>{CUP_1_8}</a>
          <a style={getLinkStyle(CUP_1_4)} onClick={() => changeCup(CUP_1_4)}>{CUP_1_4}</a>
        </div>

        <div style={blocksContainerStyle}>
          {combinations.map((comb, i) => {
            return <ResultSnap comb={comb} key={i} />;
          })}
        </div>

        <div style={summuryStyle}>
          <span> user 1</span>
          <span> user 2</span>
          <span> draw</span>
        </div>
        <div style={summuryStyle}>
          <span> {userStats.user1}</span>
          <span> {userStats.user2}</span>
          <span> {userStats.draw}</span>
        </div>
        <div style={summuryStyle}>
          <span> {parseInt(userStats.user1 / userStats.total * 100)}%</span>
          <span> {parseInt(userStats.user2 / userStats.total * 100)}%</span>
          <span> {parseInt(userStats.draw / userStats.total * 100)}%</span>
        </div>
      </div>
    );
  }
}

export default App;
