export default class BetSet {
  constructor(userName, teams) {
    this.userName = userName
    this.stages = []
    this.teams = teams
  }

  getStage(stageNo) {
    stageNo = stageNo || 0
    if(!this.stages[stageNo]) {
      this.stages[stageNo] = []
    }
    return this.stages[stageNo]
  }

  addBet(winner, stageNo) {
    const stage = this.getStage(stageNo)
    stage.push(winner)
  }

  getBets() {
    const res = []
    for(let stage of this.stages) {
      for(let name of stage) {
        const index = this.teams.indexOf(name)
        res.push(index)
      }
    }
    return res
  }
}