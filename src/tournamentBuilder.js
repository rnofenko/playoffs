const buildZeroStage = (teams) => {
  const res = []
  const count = teams.length / 2
  for(let i=0; i < count; i++) {
    res.push({ p1: teams[i*2], p2: teams[i*2 + 1]})
  }
  return res
}

const buildEmptyStage = (teamsCount) => {
  const res = []
  const count = teamsCount / 2
  for(let i=0; i < count; i++) {
    res.push({ p1: '?', p2: '?'})
  }
  return res
}

export const buildStages = (teams) => {  
  const zeroStage = buildZeroStage(teams)
  const stages = [ zeroStage ]
  let stage = zeroStage

  while(stage.length > 1) {
    stage = buildEmptyStage(stage.length)
    stages.push(stage)
  }

  return stages
}

export const mixBets = (results, bets) => {
  let i = 0
  return results.map(r => {
    if(r) {
      return r
    }
    return bets[i++]
  })
}