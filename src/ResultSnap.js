import React, { Component } from 'react';

class ResultSnap extends Component {
  render() {
    const { comb } = this.props
    const stage0 = comb.getTeams(0)
    const stage1 = comb.getTeams(1)
    const stage2 = comb.getTeams(2)
    const stage3 = comb.getTeams(3)
    const { users, points } = comb

    let blockColor = '#feffe9'
    if (users[0].points > users[1].points) {
      blockColor = '#e65f76'
    } else if (users[0].points < users[1].points) {
      blockColor = '#93e0a8'
    }

    const blockStyle = {
      margin: 10,
      width: 200,
      borderColor: blockColor,
      borderWidth: 3,
      borderStyle: 'solid'
    }
    const treeStyle = {
      display: 'flex',
      justifyContent: 'flex-start'
    }
    const colStyle = {
      justifyContent: 'space-evenly',
      display: 'flex',
      flexDirection: 'column',
      marginRight: 3
    }
    const itemsStyles = [
      { backgroundColor: '#feffe9' },
      { backgroundColor: '#e65f76' },
      { backgroundColor: '#93e0a8' }
    ]

    return (
      <div style={blockStyle}>
        <div style={treeStyle}>
          <div style={colStyle}>
            {stage0.map((team, i) => {
              return <div key={i} style={itemsStyles[points[i]]}>{team}</div>
            })}
          </div>
          <div style={colStyle}>
            {stage1.map((team, i) => {
              return <div key={i} style={itemsStyles[points[i + stage0.length]]}>{team}</div>
            })}
          </div>
          <div style={colStyle}>
            {stage2.map((team, i) => {
              return <div key={i} style={itemsStyles[points[i + stage0.length + stage1.length]]}>{team}</div>
            })}
          </div>
          {stage3 &&
            <div style={colStyle}>
              {stage3.map((team, i) => {
                return <div key={i} style={itemsStyles[points[i + stage0.length + stage1.length + stage2.length]]}>{team}</div>
              })}
            </div>
          }
        </div>
        <div>
          <span>{users[0].userName}:</span>
          <span>{users[0].points} </span>
          <span>{users[1].userName}:</span>
          <span>{users[1].points}</span>
        </div>
      </div>
    );
  }
}

export default ResultSnap
