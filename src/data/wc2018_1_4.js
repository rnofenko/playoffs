import Tournament from '../Tournament'
import BetSet from '../BetSet'

const Uruguay = 'Uruguay'
const France = 'France'
const Brazil = 'Brazil'
const Belgium = 'Belgium'
const Russia = 'Russia'
const Croatia = 'Croatia'
const Sweden = 'Sweden'
const England = 'England'

const teams = [ Uruguay, France, Brazil, Belgium, Russia, Croatia, Sweden, England ]
const fact = new Tournament(teams)

fact.addWinner(France)
fact.addWinner(Belgium)
fact.addWinner(Croatia)
fact.addWinner(England)

const roma = new BetSet('roma', teams)
roma.addBet(France)
roma.addBet(Brazil)
roma.addBet(Croatia)
roma.addBet(England)

roma.addBet(Brazil, 1)
roma.addBet(England, 1)

roma.addBet(Brazil, 2)

const yan = new BetSet('yan', teams)
yan.addBet(Uruguay)
yan.addBet(Brazil)
yan.addBet(Russia)
yan.addBet(England)

yan.addBet(Uruguay, 1)
yan.addBet(England, 1)

yan.addBet(Uruguay, 2)

const data = { 
  users: [ roma, yan ],
  fact
}
export default data