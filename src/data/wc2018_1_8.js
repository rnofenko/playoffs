import Tournament from '../Tournament'
import BetSet from '../BetSet'

const Uruguay = 'Uruguay'
const Portugal = 'Portugal'
const France = 'France'
const Argentina = 'Argentina'
const Brazil = 'Brazil'
const Mexico = 'Mexico'
const Belgium = 'Belgium'
const Japan = 'Japan'
const Spain = 'Spain'
const Russia = 'Russia'
const Croatia = 'Croatia'
const Denmark = 'Denmark'
const Sweden = 'Sweden'
const Switzerland = 'Switzerl'
const Colombia = 'Colombia'
const England = 'England'

const teams = [ Uruguay, Portugal, France, Argentina, Brazil, Mexico, Belgium, Japan, Spain, Russia, Croatia, Denmark, Sweden, Switzerland, Colombia, England ]
const fact = new Tournament(teams)

fact.addWinner(Uruguay)
fact.addWinner(France)
fact.addWinner(Russia)
fact.addWinner(Croatia)
fact.addWinner(Brazil)
fact.addWinner(Belgium)
fact.addWinner(Sweden)
fact.addWinner(England)

fact.addWinner(France, 1)
fact.addWinner(Belgium, 1)
fact.addWinner(Croatia, 1)
fact.addWinner(England, 1)

const roma = new BetSet('roma', teams)
roma.addBet(Portugal)
roma.addBet(France)
roma.addBet(Brazil)
roma.addBet(Belgium)
roma.addBet(Spain)
roma.addBet(Croatia)
roma.addBet(Sweden)
roma.addBet(England)

roma.addBet(Portugal, 1)
roma.addBet(Brazil, 1)
roma.addBet(Spain, 1)
roma.addBet(England, 1)

roma.addBet(Brazil, 2)
roma.addBet(Spain, 2)

roma.addBet(Brazil, 3)

const yan = new BetSet('yan', teams)
yan.addBet(Uruguay)
yan.addBet(France)
yan.addBet(Brazil)
yan.addBet(Belgium)
yan.addBet(Spain)
yan.addBet(Croatia)
yan.addBet(Sweden)
yan.addBet(Colombia)

yan.addBet(France, 1)
yan.addBet(Brazil, 1)
yan.addBet(Croatia, 1)
yan.addBet(Sweden, 1)

yan.addBet(Brazil, 2)
yan.addBet(Sweden, 2)

yan.addBet(Brazil, 3)

const data = { 
  users: [ roma, yan ],
  fact
}
export default data